import sys
sys.setrecursionlimit(1100)

def check_duplicate(data, group_result, result, index=0):
    if data[index] == data[index - 1]:
        # 2 เก็บที่ Duplicate ไว้ด้วยกัน
        group_result[len(group_result) - 1].append(data[index])
        result.append(data[index])
    else:
        # 1 จัดเก็บ
        group_result.append([data[index]])

    # 3 check for exit recursion function
    if len(data) == index + 1:
        result = list(set(result))
        return
    return check_duplicate(data, group_result, result, index + 1)

# ----------------------------------------------------------------
a = [num for num in range(1, 1001)]
# add 2 and 3
a.extend([2, 3])
a.sort()
print(a)

# ----------------------------------------------------------------
group_result = []
result = []
check_duplicate(a, group_result, result)

print(result)

# ----------------------------------------------------------------
