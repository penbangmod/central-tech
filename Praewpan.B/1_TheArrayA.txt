
${Array_A} = {9, 123, 1000, ... 372, 892} ---Array Size is 1002 Number between 1 and 1000 and is not a sorted array.

Create List ${list_B} = {}
														
Create Dictionary ${dic_duplicate} = {}
													
: FOR	${index}	    IN RANGE	0	1002
										
	${value_array_a}=  Get From List	${Array_A}[${index}]
										
	${status_num}=	 Run Keyword And Return Status  

				List Should Not Contain Value   ${list_B} ${value_array_a}   ---In {list_B} Should Not Contain Value ${value_array_a}
					
	Run Keyword If	"${status_num}"=="True"	Append To List	${list_B}	${value_array_a}
	
				ELSE IF	"${status_num}"=="False"	Set To Dictionary	${dic_duplicate} [index is ${index} : value is ${value_array_a}]			
Log List	${list_B}													
Log Dictionary	${dic_duplicate}																				