*** Settings ***
Resource          ../AllKeywords.txt

*** Keywords ***
OfficeMate_OpenBrowserOfficeMate
    Open Browser    ${OfficeMateUrl}    gc
    Maximize Browser Window
    Title Should Be    ${OfficeMate_PageTitle}
    Wait Until Element Is Visible    ${OfficeMate_Popup}
    Wait Until Element Is Visible    ${OfficeMate_Popup_Cancel}
    Click Element    ${OfficeMate_Popup_Cancel}
    Capture Page Screenshot

OfficeMate_FindCalculateLowestPrice
    Wait Until Element Is Visible    ${OfficeMate_SearchBar}
    Input Text    ${OfficeMate_SearchBar}    เครื่องคิดเลข
    Click Element    ${OfficeMate_Button_Search}
    Wait Until Element Is Visible    ${OfficeMate_Grid_Product}
    Wait Until Element Is Visible    ${OfficeMate_Checkbox_Canon_Brand}
    Capture Page Screenshot
    Click Element    ${OfficeMate_Checkbox_Canon_Brand}
    Wait Until Element Is Visible    ${OfficeMate_Button_Price_Range}
    Scroll Element Into View    ${OfficeMate_Button_Price_Range}
    Drag And Drop    ${OfficeMate_Slider_Price_Max}    ${OfficeMate_Slider_Price_Min}
    Capture Page Screenshot
    Click Element    ${OfficeMate_Button_Price_Range}
    Capture Page Screenshot
    Wait Until Element Is Visible    ${OfficeMate_Grid_Product}
    ${count_product}    Get Element Count    ${OfficeMate_Grid_Product}/div
    log    ${count_product}
    [Return]    ${count_product}

OfficeMate_AddCalculatorsLowestPriceToCart
    [Arguments]    ${count_product}
    Capture Page Screenshot
    ${selected_products}    Set Variable    2
    ${products}    Set Variable    0
    : FOR    ${i}    IN RANGE    0    ${count_product}
    \    ${index}    Set Variable    1
    \    ${in_stock}    Run Keyword And Return Status    Wait Until Element Is Visible    //div[@data-testid="pnl-productGrid"]/div//div/a[@data-product-stock="In Stock"][${index}]
    \    ${index}    Run Keyword If    "${in_stock}"=="False"    Set Variable    ${index}+1
    \    ...    ELSE IF    "${in_stock}"=="True"    Set Variable    ${index}
    \    Run Keyword If    "${in_stock}"=="False"    Continue For Loop
    \    Mouse Over    //div[@data-testid="pnl-productGrid"]/div//div/a[@data-product-stock="In Stock"][${index}]
    \    ${data_product_name}    Get Text    //div[@data-testid="pnl-productGrid"]/div[${index}]//h3/a
    \    ${data_product_id}    Get Element Attribute    //div[@data-testid="pnl-productGrid"]/div[${index}]//div/a    data-product-id
    \    Wait Until Element Is Visible    //div[@data-testid="pnl-productGrid"]/div[${index}]//div/a[@data-product-stock="In Stock"]/..//following-sibling::div//div[contains(@id,"btn-addCart")]
    \    Wait Until Element Is Visible    //div[@data-testid="pnl-productGrid"]/div[${index}]//div/a[@data-product-stock="In Stock"]/..//following-sibling::div//div[contains(@id,"btn-addCart")]//span[contains(text(),"ใส่ในตะกร้า")]
    \    Click Element    //div[@data-testid="pnl-productGrid"]/div[${index}]//div/a[@data-product-stock="In Stock"]/..//following-sibling::div//div[contains(@id,"btn-addCart")]
    \    ${products}    Evaluate    ${products}+1
    \    BuiltIn.Exit For Loop If    ${products}==${selected_products}
    \    sleep    10s
    log    ${products}
    [Return]    ${data_product_name}    ${data_product_id}    ${products}

OfficeMate_VerifyItemsInCart
    [Arguments]    ${data_product_name}    ${data_product_id}    ${selected_products}
    #Go to Cart Page
    Wait Until Element Is Visible    ${OfficeMate_Icon_Cart}
    Click Element    ${OfficeMate_Icon_Cart}
    Wait Until Element Is Visible    ${OfficeMate_Button_ToCart_Page}
    Click Element    ${OfficeMate_Button_ToCart_Page}
    #Verify Page Cart
    Wait Until Element Is Visible    ${OfficeMate_Cart_Page}
    Wait Until Element Is Visible    ${OfficeMate_Items_Cart}
    Wait Until Element Is Not Visible    ${OfficeMate_Loading}    20s
    Page Should Contain Element    //div[@id="cart-page"]//div//a[text()='${data_product_name}']
    ${product_id}    Get Text    //div[@id="cart-page"]//div//a[text()='${data_product_name}']/..//following-sibling::div//span
    ${product_id}    Split String    ${product_id}    ${SPACE}
    Should Be Equal    ${data_product_id}    ${product_id[1]}
    Wait Until Element Is Visible    //input[@id="txt-AddToCartQty-${data_product_id}"]
    Page Should Contain Element    //input[@id="txt-AddToCartQty-${data_product_id}"][@value="${selected_products}"]
    Capture Page Screenshot
    Close Browser
