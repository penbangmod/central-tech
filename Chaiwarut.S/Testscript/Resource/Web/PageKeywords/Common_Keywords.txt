*** Settings ***
Resource          ../PageRepositories/Common_PageRepositories.txt
Resource          Redefine_Keywords.txt
Resource          ../PageVariables/CommonPageVariables.txt
Resource          ../Resource/Web/Language/${ar_Env}/Common_Localized.txt

*** Keywords ***
wWaitPageLoading
    [Documentation]    This Keyword used for wait page loading
    ${Loading}    BuiltIn.Run Keyword And Return Status    Page Should Contain Element    xpath=//div[contains(@class,'spinner')]    ${General Timeout}
    BuiltIn.Run Keyword If    '${Loading}'=='True'    BuiltIn.Run Keywords    Wait Until Element Is Not Visible    xpath=//div[contains(@class,'spinner')]    ${General Timeout}
    ...    AND    sleep    ${General Timeout}
    ...    ELSE    BuiltIn.Return From Keyword
    ${Loading}    BuiltIn.Run Keyword And Return Status    Page Should Contain Element    xpath=//div[contains(@class,'spinner')]    ${General Timeout}
    BuiltIn.Run Keyword If    '${Loading}'=='True'    BuiltIn.Run Keywords    Wait Until Element Is Not Visible    xpath=//div[contains(@class,'spinner')]    ${General Timeout}
    ...    AND    sleep    ${General Timeout}
    ...    ELSE    BuiltIn.Return From Keyword

wClickSendSMS
    [Documentation]    This Keyword for click send sms
    ...
    ...    *Format Keyword*
    ...
    ...    wClickSendSMS
    Wait Until Element Is Visible    ${btn_SendSMS}    ${General Timeout}
    Click Web Element    ${btn_SendSMS}
    Capture Page Screenshot

wOpenBrowserOfficeMate
    Open Browser    ${Url_OfficeMate}    ff
    Maximize Browser Window
    Wait Until Element Is Visible    ${btn_CloseBanner}    ${General Timeout}
    Click Web Element    ${btn_CloseBanner}    ${General Timeout}

wInputSearchOfficeMate
    [Arguments]    ${Text}
    Wait Until Element Is Visible    ${txtbox_SearchBar}    ${General Timeout}
    Input Web Text    ${txtbox_SearchBar}    ${Text}    ${General Timeout}
    Click Web Element    ${btn_SearchResult}    ${General Timeout}
    Wait Until Element Is Visible    ${itembox_SearchResultFirst}    ${General Timeout}

wSelectSortBy
    [Arguments]    ${SortBy}
    Wait Until Element Is Visible    ${ddl_SortBy}    ${General Timeout}
    Click Web Element    ${ddl_SortBy}    ${General Timeout}
    Click Web Element    xpath=//div[@class='_3pSVv _19-Sz F0sHG _1eAL0']//div[@id='${SortBy}']    ${General Timeout}
    Wait Until Element Is Visible    ${itembox_SearchResultFirst}    ${General Timeout}
    Click Web Element    //div[@class='_2CMU5']/div/div/div[contains(.,'${Label_CalculatorAndTools}')]    ${General Timeout}
    Wait Until Element Is Visible    ${itembox_SearchResultFirst}    ${General Timeout}
    Click Web Element    ${checkbox_BrandCanon}    ${General Timeout}
    Wait Until Element Is Visible    ${itembox_SearchResultFirst}    ${General Timeout}

wSelectIntoCart
    [Arguments]    ${Quantity}
    : FOR    ${index}    IN RANGE    1    ${Quantity}+1
    \    Mouse Over    xpath=//div[@class='yrjND'][${index}]
    \    Wait Until Element Is Visible    xpath=//div[@class='yrjND'][${index}]//span[contains(@class,'AddToCartButton')]    ${General Timeout}
    \    Click Web Element    xpath=//div[@class='yrjND'][${index}]//span[contains(@class,'AddToCartButton')]    ${General Timeout}
    \    Wait Until Element Is Not Visible    //div[@class='yrjND'][${index}]//span[contains(.,'${Button_Adding}')]    ${General Timeout}
    sleep    1s
    ${Actual}    Get Web Text    ${lbl_Quantity}    ${General Timeout}
    BuiltIn.Should Be Equal As Strings    ${Quantity}    ${Actual}
    [Return]    ${Actual}

wVerifyCart
    [Arguments]    ${ItemActual}
    Click Web Element    ${btn_MiniCart}    ${General Timeout}
    Wait Until Element Is Visible    ${btn_ViewCart}    ${General Timeout}
    Click Web Element    ${btn_ViewCart}    ${General Timeout}
    ${Sum}    BuiltIn.Set Variable    0
    Wait Until Element Is Visible    ${lbl_AddToCartQuantity}    ${General Timeout}
    ${Count}    SeleniumLibrary.Get Element Count    ${lbl_AddToCartQuantity}
    : FOR    ${index}    IN RANGE    1    ${Count}+1
    \    ${Item}    Get Web Value    ${lbl_AddToCartQuantity}    ${General Timeout}
    \    ${Sum}    BuiltIn.Evaluate    ${Sum}+${Item}
    Capture Page Screenshot
    ${ItemActual}    BuiltIn.Convert To Integer    ${ItemActual}
    BuiltIn.Should Be Equal    ${Sum}    ${ItemActual}

wSelectLanguage
    [Arguments]    ${ar_Env}
    ${ar_Env}    Convert To Lowercase    ${ar_Env}
    BuiltIn.Run Keyword If    '${ar_Env}'=='th'    Click Web Element    //div[@class='_5cAvb']//div[contains(.,'${ar_Env}')]    ${General Timeout}
    ...    ELSE    Click Web Element    //div[@class='_5cAvb']//div[contains(.,'${ar_Env}')]    ${General Timeout}

MockupListData
    [Arguments]    ${DuplicateNum1}    ${DuplicateNum2}    ${AllNumber}
    ${ListNumber}    BuiltIn.Create List
    ${Quantity}    BuiltIn.Set Variable    ${AllNumber}
    : FOR    ${index}    IN RANGE    1    ${Quantity}+1
    \    Append To List    ${ListNumber}    ${index}
    ${MockDupNum1}    BuiltIn.Convert To Integer    ${DuplicateNum1}
    ${MockDupNum2}    BuiltIn.Convert To Integer    ${DuplicateNum2}
    Append To List    ${ListNumber}    ${MockDupNum1}    ${MockDupNum2}
    Log    ${ListNumber}
    [Return]    ${ListNumber}

FindDuplicateNumber
    [Arguments]    ${ListNumber}
    ${RealDupNumber}    BuiltIn.Create List
    ${Count_List}    BuiltIn.Get Length    ${ListNumber}
    ${Range}    BuiltIn.Evaluate    ${Count_List}-1
    log    ${Count_List}
    : FOR    ${index}    IN RANGE    1    ${Range}
    \    ${DupNumber}    GetDuplicateNumber    ${index}    ${Count_List}    ${ListNumber}
    \    ${CountDuplicate}    BuiltIn.Get Length    ${DupNumber}
    \    ${QuantityDuplicate}    BuiltIn.Run Keyword If    '${CountDuplicate}'=='2'    BuiltIn.Set Variable    ${index}
    \    ...    ELSE    BuiltIn.Set Variable    0
    \    Append To List    ${RealDupNumber}    ${QuantityDuplicate}
    Remove Values From List    ${RealDupNumber}    0
    log    ${RealDupNumber}
    [Return]    ${RealDupNumber}

GetDuplicateNumber
    [Arguments]    ${number}    ${Count_List}    ${ListNumber}
    ${ListGetDupNumber}    BuiltIn.Create List
    ${Range}    BuiltIn.Evaluate    ${Count_List}-1
    : FOR    ${index}    IN RANGE    0    ${Count_List}
    \    log    ${number}
    \    log    ${ListNumber[${index}]}
    \    ${Numdup}    BuiltIn.Run Keyword If    '${number}'=='${ListNumber[${index}]}'    BuiltIn.Set Variable    ${number}
    \    ...    ELSE    BuiltIn.Set Variable    0
    \    Append To List    ${ListGetDupNumber}    ${Numdup}
    \    BuiltIn.Run Keyword If    '${index}'=='${Range}'    log    ${ListGetDupNumber}
    \    BuiltIn.Run Keyword If    '${index}'=='${Range}'    Remove Values From List    ${ListGetDupNumber}    0
    \    log    ${ListGetDupNumber}
    \    ${CountNumReal}    BuiltIn.Get Length    ${ListGetDupNumber}
    \    ${NumDupReal}    BuiltIn.Run Keyword If    '${CountNumReal}'=='2'    BuiltIn.Set Variable    ${number}
    [Return]    ${ListGetDupNumber}
