*** Variables ***
#### Button ####
${btn_ProductResult_Cart}    xpath=//div[@data-testid='btn-MiniCart']
${btn_Cart_ViewCart}    xpath=//div[@class='F6P13']
#### Image ####
#### TextBox ####
#### Text ####
${txt_ProductResult_Price}    xpath=//div[@data-testid='pnl-productGrid']//div[@class='_3hiFF SkU66 g4uSJ SkU66 _3q9JJ']
${txt_ProductResult_QtyCart}    xpath=//span[@id='lbl-minicartQty']
${txt_ShoppingCart_ProductCode}    xpath=//div[@class='_3YFXD _2Ib50']//div[@class='_269qX _1TSBW I2ppM']//span[@class='_34y6c']/span
#### Lable #####
${label_ThisSiteNotSecure}    xpath=//div[@id='contentContainer']//div[@id='moreInformationAlign']//span[@id="moreInfoContainer"]/a
#### Popup ####
${popup_home_WindowPopup}    xpath=//div[@id='pnl-SubScription-Popup']//div/span[@class='_2p_6G']
